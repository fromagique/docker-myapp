#!/bin/sh
cd /myapp/myapp
exec gunicorn myapp.wsgi:application -w 2 -k gevent -b 0.0.0.0:8000

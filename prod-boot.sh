#!/bin/sh
REMOTE_IMAGE=registry.gitlab.com/fromagique/docker-myapp
docker pull ${REMOTE_IMAGE}
docker stop myapp
docker rm myapp
docker rmi myapp
docker tag ${REMOTE_IMAGE} myapp
docker rmi ${REMOTE_IMAGE}
docker run --name myapp -p 8090:8000 -d myapp


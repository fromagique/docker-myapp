FROM phoikoi/alpine-python3-web

WORKDIR /myapp
COPY . /myapp

RUN pip3 install --upgrade -r requirements.txt

EXPOSE 8000
CMD ["/myapp/run-gunicorn.sh"]

